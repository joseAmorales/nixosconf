#
# ~/.bashrc
#
#   O  O
#    /\
#  /||||\
#  \||||/
#

#set -o vi                     # Activate vi mode with <Escape>
HISTSIZE= HISTFILESIZE=        # Infinite history
export HISTCONTROL=ignoreboth
export EDITOR=vim
#export QT_QPA_PLATFORMTHEME="qt5ct"
#export PATH=$PATH:~/.dotnet/tools

export PATH=$PATH:$HOME/.local/bin

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


alias g='grep -i'
alias graph='git log --all --decorate --oneline --graph'
alias hibernate='systemctl hibernate'
alias l='less -i'
alias la='ls -A'
alias ll='ls -lh'
alias ls='ls -F --color=auto'
alias ps='ps axc | less'
alias psg='ps -e | grep -i'
alias ts='date +"%Y-%m-%d_%H-%M-%S"'

alias tma='transmission-remote -tactive -l'
alias tmd='transmission-daemon'
alias tml='transmission-remote -l | less -i'
alias tmr='transmission-remote'

alias nxc='sudo vim /etc/nixos/configuration.nix'
alias nxlg='sudo nix-env --list-generations -p /nix/var/nix/profiles/system'
alias nxrs='sudo nixos-rebuild switch'

alias yd='youtube-dl'

alias ns='nix-shell'

alias cdwdh='cd /mnt/wd/home'
alias cdh='cd ~/code/hs'


PS1='\[\e[01;36m\]\u@\h\[\e[00m\] \[\e[01;34m\]\w\[\e[00m\] $\n'
if [[ -n "$IN_NIX_SHELL" ]]; then
  PROMPT_COMMAND='export PS1="\[\e[01;32m\]nix-shell\[\e[01;34m\] \[\e[01;34m\]\w\[\e[00m\] \[\e[01;32m\]$\n\e[00m\]"
  unset PROMPT_COMMAND'
fi


#time ponysay --ponyonly
time ponytmp
